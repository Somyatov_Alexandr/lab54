import React, { Component } from 'react';
import './App.css';
import Card from "./Card/Card";
import WinCalculator from './WinCalculator';

const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
const suits = ['H', 'D', 'C', 'S'];


class App extends Component {
  state = {
    cards: [],
    outcome: ''
  };

  shuffleCards = () => {
    const cardPack = [];

    for (let s = 0; s < suits.length; s++) {
      for (let r = 0; r < ranks.length; r++) {
        let card = {suit: suits[s], rank: ranks[r]};
        cardPack.push(card);
      }
      
    }

    const cards = [];

    for (let c = 0; c < 5; c++) {
      let randomCardIndex = Math.floor(Math.random() * cardPack.length);
      let [randomCard] = cardPack.splice(randomCardIndex, 1);
      // console.log(randomCard);
      cards.push(randomCard);
    }

    const calc = new WinCalculator(cards);
    const outcome = calc.getBestHand();

    this.setState({cards, outcome});
  };

  render() {
    return (
      <div className="App playingCards faceImages card-wrap">
        <div className="card__btn-wrap">
          <button className="card__btn" onClick={this.shuffleCards}>Shuffle cards</button>
        </div>
        <div className="table">
          {
            this.state.cards.map((card, index) => {
              return (
                  <Card rank={card.rank} suit={card.suit} key={index} />
              );
            })
          }
        </div>
        <div className="outcome">
          {this.state.outcome}
        </div>
      </div>
    );
  }
}

export default App;
