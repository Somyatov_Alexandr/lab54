import React from "react";

const suits = {
  H: {className: 'hearts', symbol: '♥'},
  D: {className: 'diams', symbol: '♦'},
  C: {className: 'clubs', symbol: '♣'},
  S: {className: 'spades', symbol: '♠'}
};

// const ranks = {}


const Card = (props) => {

  const suitClass = suits[props.suit].className;
  const cardClass = `card rank-${props.rank.toLowerCase()} ${suitClass}`;
  const symbol = suits[props.suit].symbol;

  return (
    <div className={cardClass}>
        <span className="rank">{props.rank}</span>
        <span className="suit">{symbol}</span>
    </div>
  )
};

export default Card;