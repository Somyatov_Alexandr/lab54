import WinCalculator, {OUTCOMES} from "./WinCalculator";


const royalFlush = [
  {suit: 'H', rank: '10'},
  {suit: 'H', rank: 'A'},
  {suit: 'H', rank: 'J'},
  {suit: 'H', rank: 'Q'},
  {suit: 'H', rank: 'K'}
];

const flush = [
  {suit: 'H', rank: '2'},
  {suit: 'H', rank: 'A'},
  {suit: 'H', rank: 'J'},
  {suit: 'H', rank: '10'},
  {suit: 'H', rank: 'K'}
];

const pair = [
  {suit: 'H', rank: '2'},
  {suit: 'C', rank: 'A'},
  {suit: 'S', rank: 'J'},
  {suit: 'D', rank: '2'},
  {suit: 'H', rank: 'K'}
];

const three = [
  {suit: 'H', rank: '8'},
  {suit: 'C', rank: '8'},
  {suit: 'S', rank: '8'},
  {suit: 'D', rank: '2'},
  {suit: 'H', rank: 'K'}
];
 

it('should determine royal flush', () => {
  const calc = new WinCalculator(royalFlush);
  const result = calc.getBestHand();

  expect(result).toEqual(OUTCOMES.ROYAL_FLUSH);
});

it('should determine flush', () => {
  const calc = new WinCalculator(flush);
  const result = calc.getBestHand();

  expect(result).toEqual(OUTCOMES.FLUSH);
});

it('should determine a pair', () => {
  const calc = new WinCalculator(pair);
  const result = calc.getBestHand();

  expect(result).toEqual(OUTCOMES.PAIR);
});

it('should determine a three', () => {
  const calc = new WinCalculator(three);
  const result = calc.getBestHand();

  expect(result).toEqual(OUTCOMES.THREE);
});