export const OUTCOMES = {
  ROYAL_FLUSH: 'Royal flush',
  FLUSH: 'Flush',
  PAIR: 'A Pair',
  TWO_PAIR: 'Two Pair',
  THREE: 'Three',
  NOTHING: 'Nothing'
};

class WinCalculator {
  constructor(cards) {
    this.cards = cards;
    
    this.suits = this.cards.map((card) => card.suit);
    this.ranks = this.cards.map((card) => card.rank);
  
    this.isFlush = this.suits.every(suit => suit === this.suits[0]);
  }

  isRoyalFlush() {
    return this.isFlush &&
    this.ranks.includes('10') &&
    this.ranks.includes('J') &&
    this.ranks.includes('Q') &&
    this.ranks.includes('K') &&
    this.ranks.includes('A');
  }

  isPair() {
    const ranksNumber = {};

    this.ranks.forEach(rank => {
      if (!ranksNumber[rank]) {
        ranksNumber[rank] = 1;
      } else {
        ranksNumber[rank]++;
      }
    });

    return Object.values(ranksNumber).includes(2);
  }
  isThree() {
    const ranksNumber = {};

    this.ranks.forEach(rank => {
      if (!ranksNumber[rank]) {
        ranksNumber[rank] = 1;
      } else {
        ranksNumber[rank]++;
      }
    });

    return Object.values(ranksNumber).includes(3);
  }

  getBestHand(){

    if(this.isRoyalFlush()) {
      return OUTCOMES.ROYAL_FLUSH;  
    } else if(this.isFlush) {
      return OUTCOMES.FLUSH;
    } else if(this.isPair()) {
      return OUTCOMES.PAIR;
    } else if(this.isThree()) {
      return OUTCOMES.THREE;
    } else  {
      return OUTCOMES.NOTHING;
    }
  }

}

export default WinCalculator;